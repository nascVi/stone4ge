import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyDXkcyA_xbOWNSHAvl5R9o4eCPdMnWSI5s",
    authDomain: "shoes-db.firebaseapp.com",
    databaseURL: "https://shoes-db.firebaseio.com",
    projectId: "shoes-db",
    storageBucket: "",
    messagingSenderId: "451435196360",
    appId: "1:451435196360:web:201606a290542dd9"
}

export const createUserProfileDoc = async (userAuth, additionalData) => {
    if(!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`)

    const snapShot = await userRef.get()

    if(!snapShot.exists) {
        const { displayName, email } = userAuth
        const createdAt = new Date()

        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        } catch (error) {
            console.log('error creating user', error.message)
        }
    }

    return userRef
}

firebase.initializeApp(config)

export const auth = firebase.auth()

export const firestore = firebase.firestore()

const provider = new firebase.auth.GoogleAuthProvider()
provider.setCustomParameters({ prompt: 'select_account '})
export const signInWithGoogle = () => auth.signInWithPopup(provider)

export default firebase