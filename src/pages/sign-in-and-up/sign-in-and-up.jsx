import React from 'react'
import SignIn from '../../components/sign-in/sign-in'
import SignUp from '../../components/sign-up/sign-up'

import './sign-in-and-up.styles.scss'

const SignInAndUp = () => {
    return (
        <div className='sign-in-and-up'>
            <SignIn />
            <SignUp />
        </div>
    )
}

export default SignInAndUp
